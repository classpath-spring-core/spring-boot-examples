package com.classpath.assetmgmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssetMgmtApplication {
    public static void main(String[] args) {
        SpringApplication.run(AssetMgmtApplication.class, args);
    }
}
